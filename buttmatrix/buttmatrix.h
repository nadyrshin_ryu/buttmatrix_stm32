//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _BUTTMATRIX_H
#define _BUTTMATRIX_H

#include "../types.h"


// ������� ����������
#define BUTTMATRIX_pcf8574_mode         0    // ������ � ��������� ����������� ����� i2c-gpio-����������� �� pcf8574 (����� - ��������)

#if (BUTTMATRIX_pcf8574_mode)      // ���� �������� ����� i2c-gpio-����������� �� pcf8574        
  // i2c-����� ���������� pcf8574 ������� ����������� �� ����� �����������:
  // 0x20-0x27 ��� pcf8574 � 0x38-0x3F ��� pcf8574a.
  // ���������� ��������� - ������ ���. 1
  #define BUTTMATRIX_pcf8574_addr       0x27    // 7-������ i2c-����� ���������� pcf8574(a)      
  #define BUTTMATRIX_pcf8574_i2c_periph I2C1    // I2C1 ��� I2C2        
#else                           // ���� �������� �������� ����� GPIO ����������������
  //// ���� ����������������, 8 ����� �������� ������������ ��� ����������� ���������� � ����������������
  #define BUTTMATRIX_Port               GPIOA
#endif


#define BUTTMATRIX_Col0_Mask            (1 << 0)
#define BUTTMATRIX_Col1_Mask            (1 << 1)
#define BUTTMATRIX_Col2_Mask            (1 << 2)
#define BUTTMATRIX_Col3_Mask            (1 << 3)
#define BUTTMATRIX_Row0_Mask            (1 << 4)
#define BUTTMATRIX_Row1_Mask            (1 << 5)
#define BUTTMATRIX_Row2_Mask            (1 << 6)
#define BUTTMATRIX_Row3_Mask            (1 << 7)

#define BUTTMATRIX_Rows_Mask            (BUTTMATRIX_Row0_Mask | BUTTMATRIX_Row1_Mask | BUTTMATRIX_Row2_Mask | BUTTMATRIX_Row3_Mask)
#define BUTTMATRIX_Cols_Mask            (BUTTMATRIX_Col0_Mask | BUTTMATRIX_Col1_Mask | BUTTMATRIX_Col2_Mask | BUTTMATRIX_Col3_Mask)
#define BUTTMATRIX_AllPins_Mask         (BUTTMATRIX_Cols_Mask | BUTTMATRIX_Rows_Mask)




// ������� ��������� ��������� ������ ���������� � ���������� ��� � ���� ������� 16-��� �����
uint16_t buttmatrix_scan(void);
// ��������� ������������� ��� GPIO/I2C-����������� ��� ������ � �����������
void buttmatrix_init(void);


#endif