//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stm32f10x_gpio.h>
#include <stm32f10x_rcc.h>
#include "..\delay\delay.h"
#include "buttmatrix.h"
#include "gpio.h"
#include "i2cm.h"


#if (BUTTMATRIX_pcf8574_mode)
  #include "pcf8574.h"
  uint8_t buttmatrix_pcf8574_Value = 0;
#endif

  
// ������� ��� ���������� ��������
#if (BUTTMATRIX_pcf8574_mode)
  // ������� ��� ������ ����� I2C-����������� GPIO �� pcf8574 
  #define BUTTMATRIX_PIN_SET(val)     {buttmatrix_pcf8574_Value = val; pcf8574_write(BUTTMATRIX_pcf8574_i2c_periph, BUTTMATRIX_pcf8574_addr, buttmatrix_pcf8574_Value);}
  #define BUTTMATRIX_PIN_GET()        pcf8574_read(BUTTMATRIX_pcf8574_i2c_periph, BUTTMATRIX_pcf8574_addr)
  #define BUTTMATRIX_SetDATA_PinMode_In(mask)      {buttmatrix_pcf8574_Value |= (mask); pcf8574_write(BUTTMATRIX_pcf8574_i2c_periph, BUTTMATRIX_pcf8574_addr, buttmatrix_pcf8574_Value);}
  #define BUTTMATRIX_SetDATA_PinMode_Out(mask)     {}
#else
  // ������� ��� ������ �������� ����� GPIO ���������������� 
  #define BUTTMATRIX_SetDATA_PinMode_In(mask)   gpio_SetGPIOmode_In(BUTTMATRIX_Port, mask, gpio_PullUp)
  #define BUTTMATRIX_SetDATA_PinMode_Out(mask)  gpio_SetGPIOmode_Out(BUTTMATRIX_Port, mask)
  #define BUTTMATRIX_PIN_SET(val)               {GPIO_ResetBits(BUTTMATRIX_Port, val ^ 0xFFFF); GPIO_SetBits(BUTTMATRIX_Port, val);}
  #define BUTTMATRIX_PIN_GET()                  GPIO_ReadInputData(BUTTMATRIX_Port)
#endif



const uint16_t RowMasks[] = {BUTTMATRIX_Row0_Mask, BUTTMATRIX_Row1_Mask, BUTTMATRIX_Row2_Mask, BUTTMATRIX_Row3_Mask};
const uint16_t ColMasks[] = {BUTTMATRIX_Col0_Mask, BUTTMATRIX_Col1_Mask, BUTTMATRIX_Col2_Mask, BUTTMATRIX_Col3_Mask};

//==============================================================================
// ������� ��������� ��������� ������ ���������� � ���������� ��� � ���� ������� 16-��� �����
//==============================================================================
uint16_t buttmatrix_scan(void)
{
  uint16_t ReturnValue = 0;
  uint16_t ButtonsMask = 1;

  for (uint8_t row = 0; row < (sizeof(RowMasks) / sizeof(RowMasks[0])); row++)
  {
    uint16_t RowMask = RowMasks[row];
    
    BUTTMATRIX_SetDATA_PinMode_In(BUTTMATRIX_AllPins_Mask);
    BUTTMATRIX_SetDATA_PinMode_Out(RowMask);
    BUTTMATRIX_PIN_SET(RowMask ^ 0xFFFF);
    uint16_t Cols = BUTTMATRIX_PIN_GET() & BUTTMATRIX_Cols_Mask;
    
    for (uint8_t col = 0; col < (sizeof(ColMasks) / sizeof(ColMasks[0])); col++)
    {
      if (!(Cols & ColMasks[col]))
        ReturnValue |= ButtonsMask;
      
      ButtonsMask <<= 1;
    }
  }
  
  return ReturnValue;
}
//==============================================================================


//==============================================================================
// ��������� ������������� ��� GPIO/I2C-����������� ��� ������ � �����������
//==============================================================================
void buttmatrix_init(void)
{
#if (BUTTMATRIX_pcf8574_mode)
  pcf8574_bus_init(BUTTMATRIX_pcf8574_i2c_periph);
#else
  // �������� ������������ ��������� �����
  gpio_PortClockStart(BUTTMATRIX_Port);
#endif
}
//==============================================================================
